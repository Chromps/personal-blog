module.exports = {
    siteMeta: {
        title: "Words About Software and Life",
        author: "Cody Swift",
        image: "/static/site-feature.png",
        description:
            "A country boy explores the world of software engineering, and makes a lot of typos along the way.",
        blogHeader: "Stuff I have written about.",
        siteUrl: "https://www.codyswift.com",
        social: {
            twitter: "chromps"
        },
        postsPerPage: 5
    }
}
